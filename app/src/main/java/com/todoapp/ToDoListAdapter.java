package com.todoapp;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;

import com.todoapp.data.ToDoContract;

public class ToDoListAdapter extends CursorAdapter{
    public ToDoListAdapter(Context context, Cursor cursor){
        super(context,cursor);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.DoneUndoneCheckbox);
        String taskname =  cursor.getString(cursor.getColumnIndex(ToDoContract.ToDoEntry.COLUMN_NAME_TASK));
        int status = cursor.getInt(cursor.getColumnIndex(ToDoContract.ToDoEntry.COLUMN_NAME_STATUS));
        checkBox.setText(taskname);
        boolean statusbool = true;
        if (status == 0)
            statusbool = false;

        checkBox.setChecked(statusbool);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.todolistsingleitem,viewGroup,false);
    }
}
