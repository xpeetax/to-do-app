package com.todoapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.todoapp.data.ToDoContract;
import com.todoapp.data.ToDoSQLHelper;

public class ActivityListViewFragment extends Fragment {

    Cursor cursor;
    public ActivityListViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_activity_list_view, container, false);
        ListView tasks = (ListView) view.findViewById(R.id.TasksListView);
        cursor = getdata();
        ToDoListAdapter adapter = new ToDoListAdapter(getContext(), cursor);
        tasks.setAdapter(adapter);


        return view;
    }

    private Cursor getdata(){
        ToDoSQLHelper mDbHelper = new ToDoSQLHelper(getContext());
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = {
                ToDoContract.ToDoEntry._ID,
                ToDoContract.ToDoEntry.COLUMN_NAME_TASK,
                ToDoContract.ToDoEntry.COLUMN_NAME_STATUS,
        };

        Cursor c = db.query(
                ToDoContract.ToDoEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                // The sort order
        );
        return c;
    }

}
